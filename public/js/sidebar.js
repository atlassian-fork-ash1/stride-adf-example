
/**
 * Reference documentation for the Stride Javascript API: https://developer.atlassian.com/cloud/stride/apis/jsapi/about-javascript-api/
 */

$(document).ready(function() {
	function init(event) {
		$("#" + event.target.id).prop("disabled", true);
		$("#info").text("API responses will be shown here");
	}

	function success(event, data) {
		$("#" + event.target.id).prop("disabled", false);
		if (data) $("#info").text(JSON.stringify(data));
	}

	function error(event, data) {
		$("#" + event.target.id).prop("disabled", false);
		if (data) $("#info").text(JSON.stringify(data));
	}

	$("button").click(function(event) {
		var id = event.target.id;
		init(event);
		AP.auth.withToken(function(err, token) {
			$.ajax({
				type: "GET",
				url: "/"+id,
				headers: { Authorization: "Bearer " + token },
				dataType: "json",
				success: function(data) {
					success(event, data);
				},
				error: function(data) {
					error(event, data);
				}
			});
		});
	});

});