const PORT = process.env.PORT;
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const API_BASE_URL = 'https://api.atlassian.com';

if (!PORT || !CLIENT_ID || !CLIENT_SECRET) {
  console.log("Usage:");
  console.log("PORT=<http port> CLIENT_ID=<app client ID> CLIENT_SECRET=<app client secret> node app.js");
  process.exit();
}



const express = require('express');
const bodyParser = require('body-parser');
const http = require('http');
const app = express();
const jwtUtil = require('jwt-simple');
var messages = require('./helper/messages');

var { Document } = require('adf-builder');


app.use(bodyParser.json());
app.use(express.static('.'));
app.use("/public/img", express.static("public/img"));

/**
 * Adding in our routes
 */

var lifecycle = require('./routes/lifecycle');
app.use("/", lifecycle);

var advancedExample = require('./routes/advancedExample');
app.use("/advancedExample", advancedExample);

var simpleApplicationCard = require('./routes/simpleApplicationCard');
app.use("/simpleApplicationCard", simpleApplicationCard);

var advancedApplicationCard = require('./routes/advancedApplicationCard');
app.use("/advancedApplicationCard", advancedApplicationCard);

var blockquote = require('./routes/blockquote');
app.use("/blockquote", blockquote);

var bulletlist = require('./routes/bulletlist');
app.use("/bulletlist", bulletlist);

var codeblock = require('./routes/codeblock');
app.use("/codeblock", codeblock);

var decisionlist = require('./routes/decisionlist');
app.use("/decisionlist", decisionlist);

var orderedlist = require('./routes/orderedlist');
app.use("/orderedlist", orderedlist);

var tasklist = require('./routes/tasklist');
app.use("/tasklist", tasklist);

var panel = require('./routes/panel');
app.use("/panel", panel);

var table = require('./routes/table');
app.use("/table", table);

var mention = require('./routes/mention');
app.use("/mention", mention);

var headings = require('./routes/headings');
app.use("/headings", headings);

var emoji = require('./routes/emoji');
app.use("/emoji", emoji);

var media = require('./routes/media');
app.use("/media", media);


/**
 * Let's show them what we got in this sidebar.
 */
app.get('/sidebar',
  function (req, res) {
    res.redirect("/public/html/sidebar.html");
  }
);

/**
 * Return the glance status.
 */
app.get("/glance/state", (req, res) => {
  res.send(JSON.stringify({label: {value: "ADF Showcase"}}));
});

http.createServer(app).listen(PORT, function () {
  console.log('App running on port ' + PORT);
});
