const PORT = process.env.PORT;
const CLIENT_ID = process.env.CLIENT_ID;
const CLIENT_SECRET = process.env.CLIENT_SECRET;
const API_BASE_URL = 'https://api.atlassian.com';

const rp = require("request-promise");
const fs = require('fs');

let express = require("express");
let router = express.Router();

const jwtUtil = require('jwt-simple');

var { Document } = require('adf-builder');

var messages = require('../helper/messages');

/**
 * Return a media group example.
 */
router.get('/',
async function(req, res){
  const encodedJWT = req.headers["authorization"].substring(7); 
  const jwt = jwtUtil.decode(encodedJWT, null, true);
  const cloudId = jwt.context.cloudId;
  const conversationId = jwt.context.resourceId;

  // Let's first upload a file which we can then show.
  var media = fs.readFileSync('./public/img/scott-and-mike.jpg');
  var fileName = "mike-and-scott";
    var result = await UploadMedia(media,fileName,cloudId, conversationId);
    
    var doc = new Document();
    doc.mediaGroup()
      .media({
        "id": result,
        "type": "file",
        "collection": conversationId
      });

    /**
     * Let's use JSON.stringify to get a nicely formatted JSON version of our example.
     */
    var code = JSON.stringify(doc, null, 4);

    /**
     * Now we'll reset our doc and create the message showing all the information.
     */
    doc = new Document();
    doc.rule();
    doc.heading(3)
      .text("Media example:");
    doc.rule();

    doc.mediaGroup()
      .media({
        "id": result,
        "type": "file",
        "collection": conversationId
      });

    doc.rule();
    
    doc.paragraph()
      .text('Here is the JavaScript code to build the example above:');
    doc.codeBlock("javascript")
      .text(' var { Document } = require(\'adf-builder\'); \
      \n const doc = new Document(); \
      \n doc.mediaGroup() \
      \n  .media({ \
      \n    "id": mediaId, \
      \n    "type": "file", \
      \n    "collection": conversationId \
      \n  }); \
      ');

    doc.paragraph()
      .text('... and here is the example message in JSON (ADF):');
    doc.codeBlock("javascript")
      .text(code);
    
    var reply = doc.toJSON();
    sendMessage(cloudId, conversationId, reply, function (err, response) {
      if (err)
      console.log(err);
    });
  res.sendStatus(204);
});

/**
 * 
 * This function will upload our media to the Media Service.
 * It will return a mediaId we can use in our ADF code.
 * 
 */
async function UploadMedia(media, fileName, cloudId, conversationId){
  var accessToken = await oauth(CLIENT_ID, CLIENT_SECRET,API_BASE_URL);
  const uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/media?name=' + fileName;
  const options = {
    uri: uri,
    method: 'POST',
    headers: {
      authorization: "Bearer " + accessToken,
      "cache-control": "no-cache",
      "Content-Type": "application/octet-stream"
    },
    body: media
  }
  var id = rp(options, function (err, response, body) {
    if(err){
      console.log(err);
    }     
  }).then(function(parsedBody){
      var body = JSON.parse(parsedBody);
      return body.data.id;
     }).catch(function(err){
        console.log(err);
     });
  return id;
};

/**
 * Let's get ourselves an access token.
 */

async function oauth(clientId, secret, baseUrl) {
	const options = {
		uri: baseUrl + '/oauth/token',
		method: 'POST',
		json: {
			grant_type: 'client_credentials',
			client_id: clientId,
			client_secret: secret,
			audience: baseUrl.replace('https://', ''),
		},
	};

	return rp(options)
		.then(resp => {
			return resp.access_token;
		})
		.catch(err => {
			throw new Error(`unable to generate token. check your NODE_ENV is production, CLIENT_ID and CLIENT_SECRET values are correct. ${err}`);
		});
}

async function sendMessage(cloudId, conversationId, messageTxt, callback) {
  var accessToken = await oauth(CLIENT_ID,CLIENT_SECRET,API_BASE_URL);
  const uri = API_BASE_URL + '/site/' + cloudId + '/conversation/' + conversationId + '/message';
  const options = {
    uri: uri,
    method: 'POST',
    headers: {
      authorization: "Bearer " + accessToken,
      "cache-control": "no-cache"
    },
    json: {
      body: messageTxt
    }
  }
  rp(options, function (err, response, body) {
    if(err){
       console.log(err)
    }
  });
 }

module.exports = router;